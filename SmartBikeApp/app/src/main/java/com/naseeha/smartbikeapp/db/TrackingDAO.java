package com.naseeha.smartbikeapp.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.naseeha.smartbikeapp.model.Tracking;

public class TrackingDAO {
    private SQLiteDatabase db;
    private DbHelper dbHelper;
    Context context;

    public TrackingDAO(Context context) {
        this.context = context;
        dbHelper = new DbHelper(context);
    }
    public void open() throws SQLException{
        db = dbHelper.getWritableDatabase();
    }

    public int insert(Tracking tracking){
        int count = 0;
        if(db== null){
            open();
        }else if(!db.isOpen()){
            open();
        }

        try{

            ContentValues cv = new ContentValues();
            cv.put(dbHelper.COL_TRACKING_LAT,tracking.getLat());
            cv.put(dbHelper.COL_TRACKING_LONG,tracking.getLongi());
            cv.put(dbHelper.COL_TRACKING_SPEED,tracking.getSpeed());
            cv.put(dbHelper.COL_TRACKING_DATE,tracking.getDate());

            if(tracking.getLongi()!=0 && tracking.getLongi()!= 0){
                count = (int) db.insert(dbHelper.TABLE_TRACKING, null, cv);
                System.out.println("* inserted "+count);
            }else{
                System.out.println("* not inserted "+count);
            }


        }catch ( Exception e ){
            e.printStackTrace();
        }finally {
            if(db != null){
                db.close();
            }
        }

        return count;
    }
    public Tracking getFirstLocation(String date){

        Tracking tracking = new Tracking();
        if(db== null){
            open();
        }else if(!db.isOpen()){
            open();
        }
        Cursor cursor = null;
        try {
            String sql = "SELECT * FROM "+dbHelper.TABLE_TRACKING+" WHERE "+dbHelper.COL_TRACKING_DATE+" = '"+date+"'"+
                    " ORDER BY "+dbHelper.COL_TRACKING_ID+" ASC LIMIT 1";
            System.out.println("* sql "+sql);
            cursor = db.rawQuery(sql,null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()) {
                    tracking.setLat(cursor.getDouble(cursor.getColumnIndex(dbHelper.COL_TRACKING_LAT)));
                    tracking.setLongi(cursor.getDouble(cursor.getColumnIndex(dbHelper.COL_TRACKING_LONG)));
                    tracking.setSpeed(cursor.getString(cursor.getColumnIndex(dbHelper.COL_TRACKING_SPEED)));
                    tracking.setDate(cursor.getString(cursor.getColumnIndex(dbHelper.COL_TRACKING_DATE)));
                }
            }
        }catch ( Exception e ){
            e.printStackTrace();
        }finally {
            if(db != null){
                db.close();
            }
        }
        return tracking;

    }
    public Tracking getLastLocation(String date){

        Tracking tracking = new Tracking();
        if(db== null){
            open();
        }else if(!db.isOpen()){
            open();
        }
        Cursor cursor = null;
        try {
            String sql = "SELECT * FROM "+dbHelper.TABLE_TRACKING+" WHERE "+dbHelper.COL_TRACKING_DATE+" = '"+date+"'"+
                    " ORDER BY "+dbHelper.COL_TRACKING_ID+" DESC LIMIT 1";
            System.out.println("* sql "+sql);
            cursor = db.rawQuery(sql,null);
            if(cursor.getCount()>0){
                while (cursor.moveToNext()) {
                    tracking.setLat(cursor.getDouble(cursor.getColumnIndex(dbHelper.COL_TRACKING_LAT)));
                    tracking.setLongi(cursor.getDouble(cursor.getColumnIndex(dbHelper.COL_TRACKING_LONG)));
                    tracking.setSpeed(cursor.getString(cursor.getColumnIndex(dbHelper.COL_TRACKING_SPEED)));
                    tracking.setDate(cursor.getString(cursor.getColumnIndex(dbHelper.COL_TRACKING_DATE)));
                }
            }
        }catch ( Exception e ){
            e.printStackTrace();
        }finally {
            if(db != null){
                db.close();
            }
        }
        return tracking;

    }

}
