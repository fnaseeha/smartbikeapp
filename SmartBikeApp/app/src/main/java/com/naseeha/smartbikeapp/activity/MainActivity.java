package com.naseeha.smartbikeapp.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.db.TrackingDAO;
import com.naseeha.smartbikeapp.helpers.Alerts;
import com.naseeha.smartbikeapp.model.Tracking;
import com.naseeha.smartbikeapp.services.LocationService;

import static com.naseeha.smartbikeapp.activity.MapsActivity.getBatteryPercentage;
import static com.naseeha.smartbikeapp.activity.MapsActivity.getCurrentDate;
import static com.naseeha.smartbikeapp.activity.MapsActivity.round;
import static com.naseeha.smartbikeapp.helpers.Constants.CURRENT_THEME;
import static com.naseeha.smartbikeapp.helpers.NetworkConnection.checkNetworkConnection;

public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    Button navigation,finess,home;
     TextView battery,txt_heart;
    private GoogleApiClient mGoogleApiClient;
    FusedLocationProviderClient mFusedLocationProviderClient;
    private LocationManager locationManager;
    private long UPDATE_INTERVAL = 3 * 100;  /* 3 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    private LocationRequest mLocationRequest;
    Alerts alert;
    LocationService myService;
    static boolean status;
    //LocationManager locationManager;
    public static TextView dist, time, speed,txt_burn;
    Button start, pause, stop;
    public static long startTime, endTime;
    ImageView image;
    public static ProgressDialog locate;
    public static int p = 0;

    private ServiceConnection sc = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            LocationService.LocalBinder binder = (LocationService.LocalBinder) service;
            myService = binder.getService();
            status = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            status = false;
        }
    };

    void bindService() {
        if (status)
            return;
        Intent i = new Intent(getApplicationContext(), LocationService.class);
        bindService(i, sc, BIND_AUTO_CREATE);
        status = true;
        startTime = System.currentTimeMillis();
    }

    void unbindService() {
        if (!status)
            return;
        Intent i = new Intent(getApplicationContext(), LocationService.class);
        unbindService(sc);
        status = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkGps();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            //Toast.makeText(this, "GPS is Enabled in your devide", Toast.LENGTH_SHORT).show();
            return;
        }

        String number = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("LAST_MEASURE", "0");
        txt_heart.setText(number);
        //p = 0;

    }

    @Override
    protected void onStart() {
        super.onStart();

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (status)
            unbindService();
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(CURRENT_THEME.equals("Day")){
            setContentView(R.layout.activity_main);
        }else{
            setContentView(R.layout.activity_black_main);
        }

        alert = new Alerts(MainActivity.this);
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        speed = findViewById(R.id.speed);
        battery= findViewById(R.id.battery);
        battery.setText(getBatteryPercentage(MainActivity.this)+" %");
        dist = findViewById(R.id.txt_trip);

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
        navigation = findViewById(R.id.btn_nav);
        finess = findViewById(R.id.btn_fitness);
        home = findViewById(R.id.btn_home);

        time = findViewById(R.id.txt_time);

        txt_heart = findViewById(R.id.txt_heart);
        txt_burn = findViewById(R.id.txt_burn);

       // String calories_burned = (((float)((float)velocity*3.6)*((loc.getTime()/1000)- (oldLocation.getTime()/1000))*weight)/3600);



        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        startServices();

        if(speed.getText().toString().equals("26")){
            System.out.println(" * Not updated ");
            Tracking tr = new TrackingDAO(MainActivity.this).getLastLocation(getCurrentDate());
            speed.setText(tr.getSpeed());
        }
        String number = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("LAST_MEASURE", "0");
        txt_heart.setText(number);
        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = false;
                if (manager != null) {
                    statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );
                }

                if (!statusOfGPS) {
                    alert.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }


                if (!checkNetworkConnection(MainActivity.this)) {
                    alert.showInternetAlert();
                    Toast.makeText( MainActivity.this, "Please enable your Network Connection...", Toast.LENGTH_LONG ).show();
                    return;
                }

                Intent intent = new Intent(MainActivity.this, MapsActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
                finish();

            }
        });
        finess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(MainActivity.this,FitnessActivity.class);
                startActivity(f);
                finish();
            }
        });

    }

    private void startServices() {
        System.out.println("* start service");
        checkGps();
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {

            return;
        }


        if (!status)
            //Here, the Location Service gets bound and the GPS Speedometer gets Active.
            bindService();
        locate = new ProgressDialog(MainActivity.this);
        locate.setIndeterminate(true);
        locate.setCancelable(true);
        locate.setMessage("Getting Location...");
        locate.show();
    }

    void checkGps() {
        locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        if (locationManager != null && !locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {


            alert.showLocationAlert();
        }
    }

    private void enableButton() {
        Intent intent = new Intent( getApplicationContext(), LocationService.class );
        startService( intent );
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            alert.showLocationAlert();
        return isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void startLocationUpdates() {

        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {

                    float speed_m = location.getSpeed();
                    float current_speed = round(speed_m,3);
                    float kmphSpeed = round((float) (current_speed*3.6),3);
                    speed.setText(String.valueOf(kmphSpeed));

                }
            }
        });

    }
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            if (!status)
                super.onBackPressed();
            else
                moveTaskToBack(true);

        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        p = 1;
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();


        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.my_account) {
            Intent myAcc = new Intent(this,MyAccount.class);
            startActivity(myAcc);
            finish();
            // Handle the camera action
        } else if (id == R.id.my_profile) {
            Intent myprof = new Intent(this,MyProfile.class);
            startActivity(myprof);
            finish();

        } else if (id == R.id.my_goals) {
            Intent my_goal = new Intent(this,MyGoal.class);
            startActivity(my_goal);

        } else if (id == R.id.setting) {
            Intent setting = new Intent(this,Setting.class);
            startActivity(setting);
            finish();

        }
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }



    private boolean runTimePermisions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.
                checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ContextCompat.
                checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions( new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 100 );
            return true;
        }
        return false;
    }


    public void ecoClick(View view) {
        Toast.makeText(MainActivity.this,"Eco Mode Activated",Toast.LENGTH_LONG).show();
    }

    public void PowerClicked(View view) {
        Toast.makeText(MainActivity.this,"Power Mode Activated",Toast.LENGTH_LONG).show();
    }

    public void AssistClick(View view) {
        Toast.makeText(MainActivity.this,"Assist Mode Activated",Toast.LENGTH_LONG).show();
    }

    public void SmartClick(View view) {
        Toast.makeText(MainActivity.this,"Smart Mode Activated",Toast.LENGTH_LONG).show();
    }
}
