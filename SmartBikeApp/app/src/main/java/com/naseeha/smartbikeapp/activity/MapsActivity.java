package com.naseeha.smartbikeapp.activity;

import android.Manifest;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.db.TrackingDAO;
import com.naseeha.smartbikeapp.helpers.Alerts;
import com.naseeha.smartbikeapp.model.Tracking;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;


public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    private GoogleApiClient mGoogleApiClient;
    private Location mLocation;
    Alerts alerts;
    private LocationManager mLocationManager;
    private LocationRequest mLocationRequest;

    private LocationListener listener;
    private long UPDATE_INTERVAL = 2 * 1000;  /* 10 secs */
    private long FASTEST_INTERVAL = 2000; /* 2 sec */
    double lat, longi;
    private GoogleMap mMap;
    FusedLocationProviderClient mFusedLocationProviderClient;
    /* PlaceDetectionClient mPlaceDetectionClient;
     GeoDataClient mGeoDataClient;
 */
    private LocationManager locationManager;
    boolean mLocationPermissionGranted;
    TextView speed,battery_level;
    ImageView charging;
    Button home,fitness;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        getLocationPermission();
        speed = findViewById(R.id.speed);
        battery_level = findViewById(R.id.battery_level);
        charging = findViewById(R.id.charging);
        fitness = findViewById(R.id.btn_fitness);
        home = findViewById(R.id.btn_home);
        alerts = new Alerts(MapsActivity.this);

        battery_level.setText(getBatteryPercentage(MapsActivity.this)+" %");

        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, ifilter);
        int status = batteryStatus.getIntExtra(BatteryManager.EXTRA_STATUS, -1);
        boolean isCharging = status == BatteryManager.BATTERY_STATUS_CHARGING ||
                status == BatteryManager.BATTERY_STATUS_FULL;
        if(isCharging){
            charging.setImageResource(R.drawable.ic_plug);
        }else{
            charging.setImageResource(R.drawable.not_plug);
        }



        // Construct a FusedLocationProviderClient.
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        checkLocation();

        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }
                Intent intent = new Intent(MapsActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });

        fitness.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(MapsActivity.this,FitnessActivity.class);
                startActivity(f);
                finish();
            }
        });

    }

    public static int getBatteryPercentage(Context context) {

        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);
    }

    private boolean checkLocation() {
        if (!isLocationEnabled())
            showAlert();
        return isLocationEnabled();
    }

    private boolean isLocationEnabled() {
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) ||
                locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    private void showAlert() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Enable Location")
                .setMessage("Your Locations Settings is set to 'Off'.\nPlease Enable Location to " +
                        "use this app")
                .setPositiveButton("Location Settings", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        startActivity(myIntent);
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                    }
                });
        dialog.show();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        //getLocationPermission(
        if (Build.VERSION.SDK_INT >= 23) {
            runTimePermisions();
        } else {
            getLocationPermission();
        }

        // FusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }

        Task<Location> task = mFusedLocationProviderClient.getLastLocation();

        Tracking tr = new TrackingDAO(MapsActivity.this).getFirstLocation(getCurrentDate());
        LatLng startLocation = new LatLng(tr.getLat(), tr.getLongi());

        mMap.addMarker(new MarkerOptions().position(startLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cycle)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(startLocation));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLocation,
                15));

        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    //Write your implemenation here
                    LatLng curentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(curentLocation));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(curentLocation));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curentLocation,
                            15));
                    float speed_m = location.getSpeed();
                    float current_speed = round(speed_m,3);
                    float kmphSpeed = round((float) (current_speed*3.6),3);
                    speed.setText(String.valueOf(kmphSpeed));

                    //    Log.d("AndroidClarified",location.getLatitude()+" "+location.getLongitude());      }
                }
            }
        });
        // Add a marker in curentLocation and move the camera


    }

    private void getLocationPermission() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(this.getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(this,
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    1000);
        }
    }





    @Override
    public void onConnected(@Nullable Bundle bundle) {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        startLocationUpdates();

        //  mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

        //Tracking tr = new TrackingDAO(MainActivity.this).getFirstLocation(getCurrentDate());

        Tracking tr = new TrackingDAO(MapsActivity.this).getFirstLocation(getCurrentDate());
        LatLng startLocation = new LatLng(tr.getLat(), tr.getLongi());

        mMap.addMarker(new MarkerOptions().position(startLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cycle)));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(startLocation));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(startLocation,
                15));
        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
      
//.icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cycle))

        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLocation = location;
                    LatLng curentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(curentLocation));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(curentLocation));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curentLocation,
                            15));
                    float speed_m = location.getSpeed();
                    float current_speed = round(speed_m,3);
                    float kmphSpeed = round((float) (current_speed*3.6),3);
                    speed.setText(String.valueOf(kmphSpeed));
                }
            }
        });

        if(mLocation == null){
            startLocationUpdates();
        }
        if (mLocation == null) {
            Toast.makeText(this, "Loading ...", Toast.LENGTH_SHORT).show();
        }
    }

    public static String getCurrentDate() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String today = timeStampFormat.format(myDate);
        return today;
    }

    public static float round(float d, int decimalPlace) {
        BigDecimal bd = new BigDecimal(Float.toString(d));
        bd = bd.setScale(decimalPlace, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();
    }
    private void startLocationUpdates() {
       
        mLocationRequest = LocationRequest.create()
                .setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
                .setInterval(UPDATE_INTERVAL)
                .setFastestInterval(FASTEST_INTERVAL);
        // Request location updates
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }

        Task<Location> task = mFusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    mLocation = location;
                    LatLng curentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                    mMap.addMarker(new MarkerOptions().position(curentLocation).icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_cycle)));
                    mMap.moveCamera(CameraUpdateFactory.newLatLng(curentLocation));
                    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(curentLocation,
                            15));
                    float speed_m = location.getSpeed();
                    float current_speed = round(speed_m,3);
                    float kmphSpeed = round((float) (current_speed*3.6),3);
                    speed.setText(String.valueOf(kmphSpeed));

                }
            }
        });


    }

    @Override
    public void onConnectionSuspended(int i) {
        Log.i("con", "Connection Suspended");
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(this,"Connection Failed "+connectionResult.getErrorMessage(),Toast.LENGTH_LONG).show();
    }

    private boolean runTimePermisions() {
        if (Build.VERSION.SDK_INT >= 23 && ContextCompat.
                checkSelfPermission( this, android.Manifest.permission.ACCESS_FINE_LOCATION ) != PackageManager.PERMISSION_GRANTED && ContextCompat.
                checkSelfPermission( this, android.Manifest.permission.ACCESS_COARSE_LOCATION ) != PackageManager.PERMISSION_GRANTED) {
            requestPermissions( new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION}, 100 );
            return true;
        }
        return false;
    }
    //    @Override
//    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult( requestCode, permissions, grantResults );
//        if (requestCode == 100) {
//            if (grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {
//                // enableService
//            } else {
//                runTimePermisions();
//            }
//        }
//    }
//    @Override
//    public void onLocationChanged(Location location) {
//
//        String msg = "Updated Location: " +
//                Double.toString(location.getLatitude()) + "," +
//                Double.toString(location.getLongitude());
//        lat = location.getLatitude();
//        longi = location.getLongitude();
//
//        Toast.makeText(this, msg, Toast.LENGTH_SHORT).show();
//        // You can now create a LatLng Object for use with maps
//        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
//    }
//
    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    //    finish();
    }
}
