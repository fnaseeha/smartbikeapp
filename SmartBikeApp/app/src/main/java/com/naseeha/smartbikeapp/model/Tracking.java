package com.naseeha.smartbikeapp.model;

import java.io.Serializable;

public class Tracking implements Serializable {

    private double lat;
    private double longi;
    private String speed;
    private String date;

    public Tracking(double lat, double longi, String speed, String date) {
        this.setLat(lat);
        this.setLongi(longi);
        this.setSpeed(speed);
        this.setDate(date);
    }

    public Tracking() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLongi() {
        return longi;
    }

    public void setLongi(double longi) {
        this.longi = longi;
    }


    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
