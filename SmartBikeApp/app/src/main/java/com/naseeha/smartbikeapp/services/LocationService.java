package com.naseeha.smartbikeapp.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Binder;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.naseeha.smartbikeapp.activity.MainActivity;
import com.naseeha.smartbikeapp.activity.MyProfile;
import com.naseeha.smartbikeapp.db.TrackingDAO;
import com.naseeha.smartbikeapp.model.Tracking;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.*;

public class LocationService extends Service  implements
        LocationListener,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnSuccessListener<Void> {


    private static final long INTERVAL = 1000 * 2;
    private static final long FASTEST_INTERVAL = 1000 * 1;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mCurrentLocation, lStart, lEnd;
    static double distance = 0;
    double speed;
    FusedLocationProviderClient mFusedLocationProviderClient;

    private final IBinder mBinder = new LocalBinder();
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        createLocationRequest();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();
        mGoogleApiClient.connect();
        return mBinder;
    }

    @Override
    public void onLocationChanged(Location location) {
        System.out.println("* location changed");
        MainActivity.locate.dismiss();
        mCurrentLocation = location;
        if (lStart == null) {
            lStart = mCurrentLocation;
            lEnd = mCurrentLocation;
        } else
            lEnd = mCurrentLocation;

        //Calling the method below updates the  live values of distance and speed to the TextViews.
        updateUI();
        //calculating the speed with getSpeed method it returns speed in m/s so we are converting it into kmph
        speed = location.getSpeed() * 18 / 5;

        //// String calories_burned = (((float)((float)velocity*3.6)*((loc.getTime()/1000)- (oldLocation.getTime()/1000))*weight)/3600);

    }

    private void updateUI() {
        System.out.println("* update ui");
        if (MainActivity.p == 0) {
            distance = distance + (lStart.distanceTo(lEnd) / 1000.00);
            MainActivity.endTime = System.currentTimeMillis();
            long diff = MainActivity.endTime - MainActivity.startTime;
            diff = MILLISECONDS.toMinutes(diff);
            MainActivity.time.setText("Total Time: " + diff + " minutes");
            if (speed > 0.0)
                MainActivity.speed.setText(new DecimalFormat("#.##").format(speed)+ "");
            else
                MainActivity.speed.setText("0.0");

            MainActivity.dist.setText(new DecimalFormat("#.###").format(distance)+" km");

           // String weight = //MyProfile.txt_weight.getText().toString();

           // int we = Integer.parseInt(weight.substring(0,weight.length()-2));
            // String calories_burned = (((float)((float)velocity*3.6)*((loc.getTime()/1000)- (oldLocation.getTime()/1000))*weight)/3600);
            int weight =70;
             float calories_burned = (((float)(speed*(diff)*weight)/3600));
            System.out.println("* calories_burned "+calories_burned);
            MainActivity.txt_burn.setText(calories_burned+"");

            lStart = lEnd;


        }

    }

    @Override
    public boolean onUnbind(Intent intent) {
        stopLocationUpdates();
        if (mGoogleApiClient.isConnected())
            mGoogleApiClient.disconnect();
        lStart = null;
        lEnd = null;
        distance = 0;
        return super.onUnbind(intent);
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        try {
            mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
            Task<Location> task = mFusedLocationProviderClient.getLastLocation();
            task.addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    if (location != null) {
                      //  Toast.makeText(getApplicationContext(),location.getLatitude()+" "+location.getLongitude(),Toast.LENGTH_LONG).show();
                        //Write your implemenation here
                       // LatLng curentLocation = new LatLng(location.getLatitude(), location.getLongitude());
                        System.out.println("*"+"AndroidClarified"+location.getLatitude()+" "+location.getLongitude());
                          //Log.d("AndroidClarified",location.getLatitude()+" "+location.getLongitude());

                        String sp = String.valueOf( location.getSpeed() * 18 / 5);

                          Tracking tracking = new Tracking(location.getLatitude(),location.getLongitude(),sp,getCurrentDate());
                          if(new TrackingDAO(getApplicationContext()).insert(tracking)>0){
                              System.out.println("* inserted");
                          }else{
                              System.out.println("* not inserted");
                          }


                        mCurrentLocation = location;
                        if (lStart == null) {
                            lStart = mCurrentLocation;
                            lEnd = mCurrentLocation;
                        } else
                            lEnd = mCurrentLocation;
                        speed = location.getSpeed() * 18 / 5;
                        //Calling the method below updates the  live values of distance and speed to the TextViews.
                        updateUI();
                        MainActivity.locate.dismiss();
                        //calculating the speed with getSpeed method it returns speed in m/s so we are converting it into kmph

                    }
                }
            });
        } catch (SecurityException e) {
            e.printStackTrace();
        }
    }

    protected void stopLocationUpdates() {
        System.out.println("* stop location");
        mFusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        LocationCallback mLocationCallback = new LocationCallback()
        {
            @Override
            public void onLocationResult(LocationResult locationResult){
                if (locationResult != null){
                    System.out.println("location not null");
                } else {
                    System.out.println("Location request returned null");
                }
            }
        };
        Task<Void> task = mFusedLocationProviderClient.removeLocationUpdates(mLocationCallback);
        task.addOnSuccessListener(this);
//        LocationServices.FusedLocationApi.removeLocationUpdates(
//                mGoogleApiClient, this);
        distance = 0;
    }
    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        return super.onStartCommand(intent, flags, startId);
    }

    protected void createLocationRequest() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    @Override
    public void onSuccess(Void aVoid) {
        System.out.println("* location stopped");
    }

    public class LocalBinder extends Binder {

        public LocationService getService() {
            return LocationService.this;
        }
    }

    public static String getCurrentDate() {
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date myDate = new Date();
        String today = timeStampFormat.format(myDate);
        return today;
    }
}
