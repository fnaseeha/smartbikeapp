package com.naseeha.smartbikeapp.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "smartBike.db";
    public static final int DATABASE_VERSION = 1;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    //-------------------- LOCATION DETAILS TABLE -----------------
    //TABLE
    public static final String TABLE_TRACKING ="TableTracking";
    //TABLE_TRACKING COLUMNS
    public static final String COL_TRACKING_ID = "Id";
    public static final String COL_TRACKING_LAT = "Latiutde";
    public static final String COL_TRACKING_LONG = "Longitute";
    public static final String COL_TRACKING_SPEED = "Speed";
    public static final String COL_TRACKING_DATE = "Date";

    //CREATE TABLE STRING
    private static final String CREATE_TABLE_TRACKING = "CREATE TABLE IF NOT EXISTS "+TABLE_TRACKING + " ("
            + COL_TRACKING_ID +" INTEGER PRIMARY KEY AUTOINCREMENT, "
            + COL_TRACKING_LAT +" DOUBLE, "
            + COL_TRACKING_LONG+" DOUBLE, "
            + COL_TRACKING_SPEED+" TEXT, "
            + COL_TRACKING_DATE+" TEXT ); ";



    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_TRACKING);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
