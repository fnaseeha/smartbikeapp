package com.naseeha.smartbikeapp.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.helpers.Alerts;

import static com.naseeha.smartbikeapp.helpers.Constants.CURRENT_THEME;
import static com.naseeha.smartbikeapp.helpers.NetworkConnection.checkNetworkConnection;

public class Setting extends AppCompatActivity {
    TextView txt_theme;
    TextView navigation,finess,home,txt_units,reset;
    Alerts alerts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        txt_theme = findViewById(R.id.txt_theme);
        navigation = findViewById(R.id.btn_nav);
        finess = findViewById(R.id.btn_fitness);
        home = findViewById(R.id.btn_home);
        alerts = new Alerts(Setting.this);
        txt_units = findViewById(R.id.txt_units);
        reset = findViewById(R.id.reset);


        if(CURRENT_THEME.equals("Day")){
            txt_theme.setText("Day");
        }else{
            txt_theme.setText("Night");
        }
        reset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                txt_units.setText("Metrio");
                txt_theme.setText("Day");
                CURRENT_THEME = "Day";
            }
        });

        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = false;
                if (manager != null) {
                    statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );
                }

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }


                if (!checkNetworkConnection(Setting.this)) {
                    alerts.showInternetAlert();
                    Toast.makeText( Setting.this, "Please enable your Network Connection...", Toast.LENGTH_LONG ).show();
                    return;
                }

                Intent intent = new Intent(Setting.this, MapsActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = false;
                if (manager != null) {
                    statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );
                }

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }
                Intent intent = new Intent(Setting.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });

        finess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(Setting.this,FitnessActivity.class);
                startActivity(f);
                finish();
            }
        });

    
        txt_theme.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(txt_theme.getText().toString().equals("Day")){
                    txt_theme.setText("Night");
                    CURRENT_THEME = "Night";
                }else{
                    txt_theme.setText("Day");
                    CURRENT_THEME = "Day";
                }
            }
        });

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
