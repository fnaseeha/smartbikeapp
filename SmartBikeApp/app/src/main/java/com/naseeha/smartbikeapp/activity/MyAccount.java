package com.naseeha.smartbikeapp.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.helpers.Alerts;

import static com.naseeha.smartbikeapp.helpers.NetworkConnection.checkNetworkConnection;

public class MyAccount extends AppCompatActivity {

    TextView navigation,finess,home;
    Alerts alerts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_account);

        navigation = findViewById(R.id.btn_nav);
        finess = findViewById(R.id.btn_fitness);
        home = findViewById(R.id.btn_home);
        alerts = new Alerts(MyAccount.this);

        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }


                if (!checkNetworkConnection(MyAccount.this)) {
                    alerts.showInternetAlert();
                    Toast.makeText( MyAccount.this, "Please enable your Network Connection...", Toast.LENGTH_LONG ).show();
                    return;
                }

                Intent intent = new Intent(MyAccount.this, MapsActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }
                Intent intent = new Intent(MyAccount.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });
        finess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(MyAccount.this,FitnessActivity.class);
                startActivity(f);
                finish();
            }
        });

    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent f = new Intent(MyAccount.this,MainActivity.class);
        startActivity(f);
        finish();
    }
}
