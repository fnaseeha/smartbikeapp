package com.naseeha.smartbikeapp.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.LocationManager;
import android.preference.PreferenceManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.helpers.Alerts;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import lecho.lib.hellocharts.model.Axis;
import lecho.lib.hellocharts.model.AxisValue;
import lecho.lib.hellocharts.model.Line;
import lecho.lib.hellocharts.model.LineChartData;
import lecho.lib.hellocharts.model.PointValue;
import lecho.lib.hellocharts.view.LineChartView;

import static com.naseeha.smartbikeapp.helpers.NetworkConnection.checkNetworkConnection;

public class FitnessActivity extends AppCompatActivity {

    Button btn_start;
    TextView number;
    LineChartView lineChart;
    Button navigation,home;
    Alerts alerts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fitness);
        btn_start = findViewById(R.id.btn_start);
        navigation = findViewById(R.id.btn_nav);
        home = findViewById(R.id.btn_home);
        alerts = new Alerts(FitnessActivity.this);

        //number = findViewById(R.id.number);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 0);
        }
//        String numbers = PreferenceManager.getDefaultSharedPreferences(getApplicationContext()).getString("LAST_MEASURE", "0");
//        number.setText(numbers);
        drawSinAbsChart();
        btn_start.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent g = new Intent(FitnessActivity.this,Measure.class);
                startActivity(g);
            }
        });

        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }


                if (!checkNetworkConnection(FitnessActivity.this)) {
                    alerts.showInternetAlert();
                    Toast.makeText( FitnessActivity.this, "Please enable your Network Connection...", Toast.LENGTH_LONG ).show();
                    return;
                }

                Intent intent = new Intent(FitnessActivity.this, MapsActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }
                Intent intent = new Intent(FitnessActivity.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });
    }
    public void drawSinAbsChart() {

        try {
            lineChart = (LineChartView) findViewById(R.id.chart);
            String[] axisData = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sept",
                    "Oct", "Nov", "Dec"};

            int[] yAxisData = {50, 20, 15, 30, 20, 60, 15, 40, 45, 10, 90, 18};

            List<PointValue> values = new ArrayList<PointValue>();
            PointValue tempPointValue;
            for (int i = 0; i <= yAxisData.length; i++) {
                tempPointValue = new PointValue(i, yAxisData[i]);

                values.add(tempPointValue);
            }


            List<AxisValue> axisValuesForX = new ArrayList<>();
            List<AxisValue> axisValuesForY = new ArrayList<>();

            Line line = new Line(values).
                    setColor(Color.RED)
                    .setCubic(false)
                    .setHasPoints(true).setHasLabels(true);;

            for (int i = 0; i < axisData.length; i++) {
                axisValuesForX.add(i, new AxisValue(i).setLabel(axisData[i]));
            }

            for (int i = 0; i < yAxisData.length; i++) {
                axisValuesForY.add(i, new AxisValue(i).setLabel(yAxisData[i]+""));
            }

            List<Line> lines = new ArrayList<Line>();
            lines.add(line);

            LineChartData data = new LineChartData();

            Axis xAxis = new Axis(axisValuesForX);
             Axis yAxis = new Axis(axisValuesForY);
            data.setAxisXBottom(xAxis);
            data.setAxisYLeft(yAxis);

            data.setLines(lines);

            lineChart.setLineChartData(data);
        }catch ( Exception e ){
            e.printStackTrace();
        }
//////////////////////////////
//        String decimalPattern = "#.##";
//        DecimalFormat decimalFormat = new DecimalFormat(decimalPattern);
//
//        lineChart = (LineChartView) findViewById(R.id.chart);
//
//        List<PointValue> values = new ArrayList<PointValue>();
//
//        PointValue tempPointValue;
//        for (int i = 0; i <= 30; i++) {
//            tempPointValue = new PointValue(i, i+2);
////            tempPointValue.setLabel(decimalFormat
////                    .format(Math.abs((float)Math.sin(Math.toRadians(i)))));
//            values.add(tempPointValue);
//        }
//
//        Line line = new Line(values)
//                .setColor(Color.BLUE)
//                .setCubic(false)
//                .setHasPoints(true).setHasLabels(true);
//
//        List<Line> lines = new ArrayList<Line>();
//        lines.add(line);
//
//        LineChartData data = new LineChartData();
//        data.setLines(lines);
//
//        List<AxisValue> axisValuesForX = new ArrayList<>();
//        List<AxisValue> axisValuesForY = new ArrayList<>();
//        AxisValue tempAxisValue;
//
//        for (float i = 0; i <= 30; i++){
//            tempAxisValue = new AxisValue(i);
//            tempAxisValue.setLabel(i+"");
//            axisValuesForX.add(tempAxisValue);
//        }
//
//        for (float i = 0; i <= 50; i++){
//            tempAxisValue = new AxisValue(i);
//            tempAxisValue.setLabel(""+i);
//            axisValuesForY.add(tempAxisValue);
//        }
//
//        Axis xAxis = new Axis(axisValuesForX);
//        Axis yAxis = new Axis(axisValuesForY);
//        data.setAxisXBottom(xAxis);
//        data.setAxisYLeft(yAxis);
//
//
//        lineChart.setLineChartData(data);


    }

}
