package com.naseeha.smartbikeapp.activity;

import android.content.Context;
import android.content.Intent;
import android.location.LocationManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.naseeha.smartbikeapp.R;
import com.naseeha.smartbikeapp.helpers.Alerts;

import static com.naseeha.smartbikeapp.helpers.NetworkConnection.checkNetworkConnection;

public class MyProfile extends AppCompatActivity {

    TextView navigation,finess,home;
    public static TextView txt_weight;
    Alerts alerts;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);


        navigation = findViewById(R.id.btn_nav);
        finess = findViewById(R.id.btn_fitness);
        home = findViewById(R.id.btn_home);
        txt_weight = findViewById(R.id.txt_weight);
        alerts = new Alerts(MyProfile.this);

        txt_weight.setText("70kg");

        navigation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }


                if (!checkNetworkConnection(MyProfile.this)) {
                    alerts.showInternetAlert();
                    Toast.makeText( MyProfile.this, "Please enable your Network Connection...", Toast.LENGTH_LONG ).show();
                    return;
                }

                Intent intent = new Intent(MyProfile.this, MapsActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);

            }
        });
        home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                LocationManager manager  = (LocationManager) getApplicationContext().getSystemService( Context.LOCATION_SERVICE );
                boolean statusOfGPS = manager.isProviderEnabled( LocationManager.GPS_PROVIDER );

                if (!statusOfGPS) {
                    alerts.showLocationAlert();
                    Toast.makeText( getApplicationContext().getApplicationContext(), "GPS is disabled.. Please Check your GPS", Toast.LENGTH_LONG ).show();
                    return;
                }
                Intent intent = new Intent(MyProfile.this, MainActivity.class);
                intent.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK );
                startActivity(intent);
            }
        });
        finess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent f = new Intent(MyProfile.this,FitnessActivity.class);
                startActivity(f);
                finish();
            }
        });
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Intent f = new Intent(MyProfile.this,MainActivity.class);
        startActivity(f);
        finish();
    }

}
