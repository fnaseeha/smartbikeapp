package com.naseeha.smartbikeapp.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.naseeha.smartbikeapp.R;

public class BlackMainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_black_main);
    }
}
