package com.naseeha.smartbikeapp.helpers;

public class Constants {


    public static String CURRENT_THEME = "Day";
    /**
     * Suppress default constructor for noninstantiability
     */
    private Constants() {
         throw new AssertionError();
    }
}
